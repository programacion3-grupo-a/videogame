﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.BusinessLogic;

public interface IGamesService
{
    Dictionary<string, IEnumerable<GetGameDto>> GetGames();
    GetGameDto GetGame(Guid id);
    GetGameDto CreateGame(GameDto game);
    GetGameDto UpdateGame(Guid id, GameDto game);
    void DeleteGame(Guid id);
}