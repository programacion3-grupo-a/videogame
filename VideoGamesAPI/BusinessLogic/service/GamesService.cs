﻿using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.repositorty;

namespace VideoGamesAPI.BusinessLogic;

public class GamesService : IGamesService
{
    private const string GamesKey = "games";
    private readonly IGameRepository _gameRepository;

    public GamesService(IGameRepository gameRepository)
    {
        _gameRepository = gameRepository;
    }

    public Dictionary<string, IEnumerable<GetGameDto>> GetGames()
    {
        var games = _gameRepository.GetGames();
        return new Dictionary<string, IEnumerable<GetGameDto>> { { GamesKey, games } };
    }

    public GetGameDto GetGame(Guid id)
    {
        return _gameRepository.GetGame(id);
    }

    public GetGameDto CreateGame(GameDto game)
    {
        if (!_gameRepository.IsNameUnique(game.Name))
        {
            throw new InvalidOperationException("Ya existe un juego con el mismo nombre.");
        }

        return _gameRepository.AddGame(game);
    }

    public GetGameDto UpdateGame(Guid id, GameDto game)
    {
        var existingGame = _gameRepository.GetGame(id);
        if (existingGame == null)
        {
            return null;
        }

        if (!_gameRepository.IsNameUnique(game.Name, id))
        {
            throw new InvalidOperationException("Ya existe un juego con el mismo nombre.");
        }

        return _gameRepository.UpdateGame(id, game);
    }


    public void DeleteGame(Guid id)
    {
         _gameRepository.DeleteGame(id);
    }
}