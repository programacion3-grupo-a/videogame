
using Microsoft.EntityFrameworkCore;
using VideoGamesAPI.BusinessLogic;
using VideoGamesAPI.DataAccess;
using VideoGamesAPI.DataAccess.mapper;
using VideoGamesAPI.DataAccess.repositorty;

namespace VideoGamesAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddDbContext<GameDbContext>(options =>
            {
                options.UseInMemoryDatabase("GameDatabase");
            });

            builder.Services.AddAutoMapper(typeof(GameMapping));
            builder.Services.AddScoped<IGameRepository, GameRepository>();
            builder.Services.AddScoped<IGamesService, GamesService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
