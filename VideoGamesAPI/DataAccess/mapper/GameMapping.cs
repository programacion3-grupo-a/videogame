﻿using AutoMapper;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;

namespace VideoGamesAPI.DataAccess.mapper;

public class GameMapping : Profile
{
    public GameMapping()
    {
        CreateMap<GameEntity, GetGameDto>().ReverseMap();
        CreateMap<GameEntity, GameDto>().ReverseMap();
    }
}
