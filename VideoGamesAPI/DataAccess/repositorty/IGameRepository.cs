﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.DataAccess.repositorty;

/// <summary>
/// Interfaz que define las operaciones disponibles para interactuar con los juegos en el repositorio.
/// </summary>
public interface IGameRepository
{
    IEnumerable<GetGameDto> GetGames();
    GetGameDto GetGame(Guid id);
    GetGameDto AddGame(GameDto game);
    GetGameDto UpdateGame(Guid id, GameDto game);
    void DeleteGame(Guid id);
    bool IsNameUnique(string name, Guid? excludeId = null);
}