﻿namespace VideoGamesAPI.DataAccess;

using Microsoft.EntityFrameworkCore;
using VideoGamesAPI.DataAccess.model;

/// <summary>
/// Representa un DbContext para gestionar juegos.
/// </summary>
public class GameDbContext : DbContext
{
    /// <summary>
    /// Obtiene o establece el DbSet de entidades de GameEntity.
    /// </summary>
    public DbSet<GameEntity> Games { get; set; }

    /// <summary>
    /// Inicializa una nueva instancia de la clase GameDbContext con las opciones especificadas.
    /// </summary>
    /// <param name="options">Las opciones de DbContext.</param>
    public GameDbContext(DbContextOptions<GameDbContext> options) : base(options)
    {
        if (!Games.Any())
        {
            // Agregar datos predefinidos solo si no hay datos en la base de datos
            Games.AddRange(
                new List<GameEntity>
                {
                     new GameEntity { Name = "Halo Infinite", Platform = "Xbox", Price = 59.99m },
                     new GameEntity { Name = "God of War", Platform = "PlayStation", Price = 49.99m }
                }
            );
            SaveChanges();
        }
    }
}
