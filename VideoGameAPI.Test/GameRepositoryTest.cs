using AutoMapper;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;
using VideoGamesAPI.DataAccess;
using VideoGamesAPI.DataAccess.repositorty;
using Microsoft.EntityFrameworkCore;
using VideoGamesAPI.DataAccess.mapper;

namespace VideoGameAPI.Test;


public class GameRepositoryTest
{
    private readonly GameRepository _gameRepository;
    private readonly DbContextOptions<GameDbContext> options;

    public GameRepositoryTest()
    {
         options = new DbContextOptionsBuilder<GameDbContext>()
              .UseInMemoryDatabase(databaseName: "TestDatabase")
              .Options;

        var mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new GameMapping());
        }).CreateMapper();

        var dbContext = new GameDbContext(options);
        _gameRepository = new GameRepository(dbContext, mapper);
    }

    private void SeedData(IEnumerable<GameEntity> games)
    {
        using (var context = new GameDbContext(options))
        {
            context.Games.AddRange(games);
            context.SaveChanges();
        }
    }
/*
 [Fact]
    public void GetGames_ReturnsListOfGames()
    {
        var games = new List<GameEntity>
            {
                new GameEntity { Id = Guid.NewGuid(), Name = "Game 1", Platform = "Platform 1", Price = 20 },
                new GameEntity { Id = Guid.NewGuid(), Name = "Game 2", Platform = "Platform 2", Price = 25 }
            };
        SeedData(games);

        var result = _gameRepository.GetGames();

        Assert.NotNull(result);
        Assert.Equal(4, result.Count());
    }*/
    
    [Fact]
    public void GetGame_ReturnsGame()
    {
        var gameId = Guid.NewGuid();
        var game = new GameEntity { Id = gameId, Name = "Game 1", Platform = "Platform 1", Price = 20 };
        SeedData(new List<GameEntity> { game });

        var result = _gameRepository.GetGame(gameId);

        Assert.NotNull(result);
        Assert.Equal(game.Id, result.Id);
        Assert.Equal(game.Name, result.Name);
        Assert.Equal(game.Platform, result.Platform);
        Assert.Equal(game.Price, result.Price);
    }

    [Fact]
    public void AddGame_ReturnsAddedGame()
    {
        var gameToAdd = new GameDto { Name = "New Game", Platform = "Platform 3", Price = 30 };
        var result = _gameRepository.AddGame(gameToAdd);

        Assert.NotNull(result);
        Assert.True(result.Id != Guid.Empty);
        Assert.Equal(gameToAdd.Name, result.Name);
        Assert.Equal(gameToAdd.Platform, result.Platform);
        Assert.Equal(gameToAdd.Price, result.Price);
    }

    [Fact]
    public void UpdateGame_ReturnsUpdatedGame()
    {
        var existingGameId = Guid.NewGuid();
        var existingGame = new GameEntity { Id = existingGameId, Name = "Existing Game", Platform = "Platform 4", Price = 40 };
        var updatedGameDto = new GameDto { Name = "Updated Game", Platform = "Platform 5", Price = 50 };
        SeedData(new List<GameEntity> { existingGame });

        var result = _gameRepository.UpdateGame(existingGameId, updatedGameDto);

        Assert.NotNull(result);
        Assert.Equal(existingGameId, result.Id);
        Assert.Equal(updatedGameDto.Name, result.Name);
        Assert.Equal(updatedGameDto.Platform, result.Platform);
        Assert.Equal(updatedGameDto.Price, result.Price);
    }

    [Fact]
    public void DeleteGame_RemovesGame()
    {
        var gameToDelete = new GameEntity { Id = Guid.NewGuid(), Name = "Game to Delete", Platform = "Platform 6", Price = 60 };
        SeedData(new List<GameEntity> { gameToDelete });

        _gameRepository.DeleteGame(gameToDelete.Id);

        using (var context = new GameDbContext(options))
        {
            var result = context.Games.FirstOrDefault(g => g.Id == gameToDelete.Id);
            Assert.Null(result);
        }
    }
}