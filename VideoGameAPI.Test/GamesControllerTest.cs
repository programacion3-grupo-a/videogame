﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VideoGamesAPI.BusinessLogic;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.Controllers;

namespace VideoGameAPI.Test;

public class GamesControllerTests
{
    private readonly GamesController _controller;
    private readonly Mock<IGamesService> _gamesServiceMock;

    public GamesControllerTests()
    {
        _gamesServiceMock = new Mock<IGamesService>();
        _controller = new GamesController(_gamesServiceMock.Object);
    }

    [Fact]
    public void GetAll_ReturnsOkResultWithGames()
    {
        var games = new List<GetGameDto>
    {
        new GetGameDto { Id = Guid.NewGuid(), Name = "Game 1", Platform = "Platform 1", Price = 20 },
        new GetGameDto { Id = Guid.NewGuid(), Name = "Game 2", Platform = "Platform 2", Price = 30 }
    };

        var gamesDictionary = new Dictionary<string, IEnumerable<GetGameDto>> { { "games", games } };

        _gamesServiceMock.Setup(x => x.GetGames()).Returns(gamesDictionary);

        var result = _controller.GetAll();

        var okResult = Assert.IsType<OkObjectResult>(result);
        var returnedGames = Assert.IsAssignableFrom<Dictionary<string, IEnumerable<GetGameDto>>>(okResult.Value);
        Assert.Equal(gamesDictionary, returnedGames);
    }

    [Fact]
    public void GetById_ReturnsOkResultWithGame()
    {
        var gameId = Guid.NewGuid();
        var game = new GetGameDto { Id = gameId, Name = "Game 1", Platform = "Platform 1", Price = 20 };

        _gamesServiceMock.Setup(x => x.GetGame(gameId)).Returns(game);

        var result = _controller.GetById(gameId);
        var okResult = Assert.IsType<OkObjectResult>(result);
        var returnedGame = Assert.IsAssignableFrom<GetGameDto>(okResult.Value);
        Assert.Equal(game, returnedGame);
    }

    [Fact]
    public void Create_Returns201CreatedStatusCode()
    {
        var gameToAdd = new GameDto { Name = "New Game", Platform = "Platform 3", Price = 30 };
        var createdGame = new GetGameDto { Id = Guid.NewGuid(), Name = "New Game", Platform = "Platform 3", Price = 30 };
        _gamesServiceMock.Setup(x => x.CreateGame(It.IsAny<GameDto>())).Returns(createdGame);

        var result = _controller.Create(gameToAdd);

        var statusCodeResult = Assert.IsType<StatusCodeResult>(result);
        Assert.Equal(StatusCodes.Status201Created, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Update_ReturnsOkResultWithUpdatedGame()
    {
        var gameId = Guid.NewGuid();
        var updatedGameDto = new GameDto { Name = "Updated Game", Platform = "Platform 5", Price = 50 };
        var updatedGame = new GetGameDto { Id = gameId, Name = "Updated Game", Platform = "Platform 5", Price = 50 };
        _gamesServiceMock.Setup(x => x.UpdateGame(It.IsAny<Guid>(), It.IsAny<GameDto>())).Returns(updatedGame);

        var result = _controller.Update(gameId, updatedGameDto);
        var okResult = Assert.IsType<OkObjectResult>(result);
        var returnedGame = Assert.IsType<GetGameDto>(okResult.Value);
        Assert.Equal(updatedGame, returnedGame);
    }

    [Fact]
    public void Create_WithDuplicateName_ReturnsConflict()
    {
        var gameToAdd = new GameDto { Name = "God of War", Platform = "Xbox", Price = 30 };
        var gameTo = new GameDto { Name = "God of War", Platform = "Xbox", Price = 30 };

        _gamesServiceMock.SetupSequence(x => x.CreateGame(It.IsAny<GameDto>()))
            .Returns(new GetGameDto { Id = Guid.NewGuid(), Name = "God of War", Platform = "Xbox", Price = 30 }) // Return created game
            .Throws(new InvalidOperationException("Ya existe un juego con el mismo nombre.")); // Throw exception for duplicate name

        var result = _controller.Create(gameToAdd);
        var result2 = _controller.Create(gameTo);

        var conflictResult = Assert.IsType<ConflictObjectResult>(result2);
        Assert.Equal(409, conflictResult.StatusCode);
    }

    [Fact]
    public void Create_WithNullFields_ReturnsBadRequest()
    {
        var gameToAdd = new GameDto { Name = null, Platform = "Xbox", Price = 30 };
        _gamesServiceMock.Setup(x => x.CreateGame(It.IsAny<GameDto>()))
                         .Throws(new InvalidOperationException("El nombre del juego es obligatorio."));

        var result = _controller.Create(gameToAdd);

        var badRequestResult = Assert.IsType<ConflictObjectResult>(result);
        Assert.NotNull(badRequestResult);
        Assert.Equal(StatusCodes.Status409Conflict, badRequestResult.StatusCode);
    }

    [Fact]
    public void Create_WithNegativePrice_ReturnsBadRequest()
    {
        var gameToAdd = new GameDto { Name = "New Game", Platform = "Xbox", Price = -10 };
        _gamesServiceMock.Setup(x => x.CreateGame(It.IsAny<GameDto>()))
                         .Throws(new InvalidOperationException("El precio del juego debe ser mayor que cero."));

        var result = _controller.Create(gameToAdd);

        var badRequestResult = Assert.IsType<ConflictObjectResult>(result);
        Assert.NotNull(badRequestResult);
        Assert.Equal(StatusCodes.Status409Conflict, badRequestResult.StatusCode);
    }

    [Fact]
    public void Create_WithInvalidPlatform_ReturnsBadRequest()
    {
        var gameToAdd = new GameDto { Name = "New Game", Platform = "PC", Price = 30 };
        _gamesServiceMock.Setup(x => x.CreateGame(It.IsAny<GameDto>()))
                         .Throws(new InvalidOperationException("La plataforma solo puede ser Xbox o Play Station"));

        var result = _controller.Create(gameToAdd);

        var badRequestResult = Assert.IsType<ConflictObjectResult>(result);
        Assert.NotNull(badRequestResult);
        Assert.Equal(StatusCodes.Status409Conflict, badRequestResult.StatusCode);
    }
}